package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"gos/cmds"
	"gos/setting"
	"os"
)

func init() {
	mainCmd.AddCommand(VersionCmd)
	mainCmd.AddCommand(cmds.RemoveCmd)
	mainCmd.AddCommand(cmds.InfoCmd)
	mainCmd.AddCommand(cmds.BuildCmd)
}

func main() {
	if err := mainCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

var mainCmd = &cobra.Command{
	Use:   "GOS",
	Short: "GOS Command line operating system",
	Long: `
	GOS
`,
}
var VersionCmd = &cobra.Command{
	Use:   "version",
	Short: "Display GOS version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(setting.Varsion)
	},
}
