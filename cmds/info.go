package cmds

import (
	"github.com/rivo/tview"
	"github.com/spf13/cobra"
)

var InfoCmd = &cobra.Command{
	Use:   "info",
	Short: "GOS system info",
	Run: func(cmd *cobra.Command, args []string) {
		GetInfo()
	},
}

func GetInfo() {
	app := tview.NewApplication()
	//box := tview.NewBox().SetBorder(true).SetTitle(" OS Info ")
	//box.SetRect(0, 0, 100, 100)
	flex := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewBox().SetBorder(true).SetTitle(" OS info"), 0, 1, false), 0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewBox().SetBorder(true).SetTitle(" OS info"), 5, 1, false).
			AddItem(tview.NewBox().SetBorder(true).SetTitle(" Middle (3 x height of Top)"), 5, 1, false).
			AddItem(tview.NewBox().SetBorder(true).SetTitle(" Bottom (5 rows)"), 5, 1, false), 0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewBox().SetBorder(true).SetTitle(" OS info"), 5, 1, false), 0, 1, false)
	if err := app.SetRoot(flex, true).SetFocus(flex).Run(); err != nil {
		panic(err)
	}
}
