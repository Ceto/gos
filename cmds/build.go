package cmds

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"os/exec"
)

var BuildCmd = &cobra.Command{
	Use:   "build",
	Short: "GOS Golang project builder",
	Long: `gos build win,
gos build linux,
gos build mac`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.Help() // nolint
			return
		}
		fmt.Println("开始 building")
		for _, a := range args {
			switch a {
			case OS_WIN:
				fmt.Println("设置 win 环境 start")
				os.Setenv("CGO_ENABLED", "0")
				os.Setenv("GOOS", "windows")
				os.Setenv("GOARCH", "amd64")
				fmt.Println("设置 win 环境 end")
				break
			case OS_LINUX:
				fmt.Println("设置 linux 环境 start")
				os.Setenv("CGO_ENABLED", "0")
				os.Setenv("GOOS", "linux")
				os.Setenv("GOARCH", "amd64")
				fmt.Println("设置 linux 环境 end")
				break
			case OS_MAC:
				fmt.Println("设置 mac 环境 start")
				os.Setenv("CGO_ENABLED", "0")
				os.Setenv("GOOS", "darwin")
				os.Setenv("GOARCH", "amd64")
				fmt.Println("设置 mac 环境 end")
				break
			}
		}
		fmt.Println("开始打包")
		shell := exec.Command("go", "build")
		output, err := shell.CombinedOutput()
		if err != nil {
			fmt.Println(fmt.Sprint(err) + ": " + string(output))
		}
		fmt.Println("结束")
	},
}
