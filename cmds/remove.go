package cmds

import (
	"fmt"
	"github.com/spf13/cobra"
	"gos/utils"
)

var RemoveCmd = &cobra.Command{
	Use:   "rm",
	Short: "GOS remove files",
	Run: func(cmd *cobra.Command, args []string) {
		nowPath := utils.Getwd()
		var rmPath string
		if len(args) > 0 {
			rmPath = fmt.Sprintf("%s/%s", nowPath, args[0])
		} else {
			rmPath = nowPath
		}
		//rmPath := "C:\\Users\\EDY\\AppData\\Local\\Temp"
		utils.RemoveFile(rmPath)
	},
}
