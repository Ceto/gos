package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"runtime"
)

func Getwd() string {
	nowPath, _ := os.Getwd()
	return nowPath
}

func RemoveFile(filePath string) {
	num := runtime.NumCPU()
	runtime.GOMAXPROCS(num - 2)
	fmt.Println("Cpu 核心数量：", num)
	fmt.Printf("Info: Start\n")
	dir, _ := ioutil.ReadDir(filePath)
	FilesLen := len(dir)
	fmt.Printf("共计%v个文件\n", FilesLen)

	for _, d := range dir {
		delPath := path.Join([]string{filePath, d.Name()}...)
		fmt.Print(delPath + "\n")
		os.RemoveAll(delPath)
		//go dodel(delPath)
	}

	fmt.Println("删除完毕")
	return
}

func dodel(delPath string) {
	os.RemoveAll(delPath)
}
